import { useState } from "react";
import {
    Box, Heading, Text, Input, FormControl, FormHelperText, Select, Button, ButtonGroup, Center, Spinner
} from "@chakra-ui/react"
import { Card, CardHeader, CardBody, CardFooter } from '@chakra-ui/card';
import { useFormik } from "formik";
import useWallet from "../../contexts/wallet";
import { Transaction } from '@martifylabs/mesh'
import { useAssets } from '@meshsdk/react';
import { useQuery, gql } from "@apollo/client";
import { hexToString } from "../../cardano/utils";

const QUERY = gql`
    query assetsAndUtxos($address: String!) {
        utxos (where: {address: {_eq: $address}}) {
            tokens {
                asset {
                    assetName
                    policyId
                }
                quantity
            }
        }
    }
`;


export default function TransactionSura() {

    const { walletConnected, wallet, connectedAddress } = useWallet();
    const [successfulTxHash, setSuccessfulTxHash] = useState<string | null>(null)
    const [loading, setLoading] = useState(false);

    const formik = useFormik({
        initialValues: {
            address: '',
            tokenName: '',
            tokenAmount: ''
        },
        onSubmit: _ => {
            alert("Success!");
        },
    });

    const { data, loading: qLoading, error } = useQuery(QUERY, {
        variables: {
            address: connectedAddress
        }
    });

    if (qLoading) {
        return (
            <Center p='10'>
                <Spinner size='xl' speed="1.0s" />
            </Center>
        );
    };

    if (error) {
        console.error(error);
        return (
            <Heading size="lg">Error fetching wallet tokens data.</Heading>
        );
    };

    let walletTokens: any[] = []

    if (data) {
        data.utxos.forEach((utxo: any) => {
            utxo.tokens.forEach((token: any) => {
                walletTokens.push({
                    quantity: token.quantity,
                    tokenName: token.asset.assetName,
                    policyId: token.asset.policyId
                })
            })
        })
    }

    const handleTransaction  = async () => {
        if (walletConnected) {
            setLoading(true)
            
            const network = await wallet.getNetworkId()
            if (network == 1) {
                alert("For now, this dapp only works on Cardano Testnet")
            } else {
                
                try {
                    
                    const tx = new Transaction({ initiator: wallet })
                        .sendAssets(
                            formik.values.address,
                            [{
                                unit: formik.values.tokenName,
                                quantity: formik.values.tokenAmount
                            }]
                        );
                    const unsignedTx = await tx.build();
                    const signedTx = await wallet.signTx(unsignedTx);
                    const txHash = await wallet.submitTx(signedTx);
                    setSuccessfulTxHash(txHash)
                } catch (error: any) {
                    if (error.info) {
                        alert(error.info)
                    }
                    else {
                        alert(error)
                        console.log(error)
                    }
                }
                
            }
            
            setLoading(false)
        }
        else {
            alert("please connect a wallet")
        }
         
    }

    return (
      <Box
        color="#b1b1b3"
        m="5"
        p="5"
        border="1px"
        borderColor="gray.200"
        borderRadius="lg"
        bgGradient="linear(to-r, black, gray.600)"
      >
        <Heading size="lg" p="2" color="purple.400">
          Token Sender
        </Heading>
        <Heading size="sm" px="2" pb="3" textColor="#6bdaff">
          This component allows you to select any token from your connected
          wallet and send it to a recipient address.
        </Heading>
        {loading ? (
          <Center>
            <Spinner />
          </Center>
        ) : (
          <Box
            bg="#353536"
            p="5"
            border="1px"
            borderColor="gray.500"
            borderRadius="lg"
          >
            <Card>
              <CardBody fontFamily={"Calibri"} textColor={"#807e7e"}>
                <FormControl>
                  <Input
                    my="3"
                    bg="white"
                    id="address"
                    name="address"
                    onChange={formik.handleChange}
                    value={formik.values.address}
                    placeholder="Recipient Address"
                  />
                  <Select
                    placeholder="Select Token"
                    isRequired
                    bg="white"
                    mb="3"
                    onChange={formik.handleChange}
                    id="tokenName"
                    name="tokenName"
                    value={formik.values.tokenName}
                  >
                    {walletTokens.map((walletToken) => (
                      <option
                        value={walletToken.policyId + walletToken.tokenName}
                      >
                        {hexToString(walletToken.tokenName)} - (
                        {walletToken.quantity})
                      </option>
                    ))}
                  </Select>
                  <Input
                    mb="3"
                    bg="white"
                    id="tokenAmount"
                    name="tokenAmount"
                    onChange={formik.handleChange}
                    value={formik.values.tokenAmount}
                    placeholder="Enter token amount"
                    isRequired
                  />
                  <Button
                    colorScheme="purple"
                    variant="solid"
                    onClick={handleTransaction}
                  >
                    Send
                  </Button>
                </FormControl>
              </CardBody>
            </Card>
            <Box
              color="#b1b1b3"
              my="5"
              p="2"
              border="1px"
              borderColor="gray.200"
              borderRadius="lg"
              bgGradient="linear(to-r, black, teal.600)"
              width="100%"
            >
              <Heading size="sm" py="1" textColor="#b9faf4">
                Status
              </Heading>
              {successfulTxHash ? (
                <Text size="sm" fontFamily={"Calibri"} textColor={"#24e312"}>Successful tx: {successfulTxHash}</Text>
              ) : (
                <Text  fontFamily={"Calibri"} textColor={"#24e312"}>Ready to test a transaction!</Text>
              )}
            </Box>
          </Box>
        )}
      </Box>
    );
}