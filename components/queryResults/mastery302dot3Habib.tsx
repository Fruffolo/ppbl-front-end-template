import { useQuery, gql } from "@apollo/client";

import {Heading, Text, Box, Table, Thead, Tbody, Tr, Th,Td,TableContainer, } 
from "@chakra-ui/react";

// ------------------------------------------------------------------------
// Module 302, Mastery Assignment #3
//
// STEP 1: Replace this GraphQL query with a new one that you write.
//
// Need some ideas? We will brainstorm at Live Coding.
// ------------------------------------------------------------------------
/* const QUERY = gql`
    query LastTxAtAddress($address: String){
        transactions(where : {outputs : {address : {_eq : $address}}}, order_by : {includedAt : desc}, limit: 1){
                includedAt
                hash
                inputs {
                    address
                }
                outputs {
                    address
                }
            }
        }
`; */

const QUERY = gql`
  query getUtxosFromAddress($address: String!) {
    utxos(where: { address: { _eq: $address } }) {
      txHash
      index
      value
    }
  }
`;

export default function Mastery302dot3Habib() {
  const queryAddress =
    "addr_test1qrjgfl2qszhe0v29q9yvpesp4xaw84w8mr2wz555lmwqdy79ct43x57t30spkxrenalkkp25sgyjktw7exq6y3z2vlrqfp0tms";

const { data, loading, error } = useQuery(QUERY, {
    variables: {
      address: queryAddress,
    },
  });

  if (loading) {
    return <Heading size="lg">Loading data...</Heading>;
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  // ------------------------------------------------------------------------
  // Module 302, Mastery Assignment #3
  //
  // STEP 2: Style your query results here.
  //
  // This template is designed be a simple example - add as much custom
  // styling as you want!
  // ------------------------------------------------------------------------

  return (
    <Box p="3" bg="orange.100" border="1px" borderRadius="lg">
      <Heading py="2" size="md">
        This is a Template for Mastery Assignment 302.3 from Habib
      </Heading>
      <Text pt='5'>UTXOs for Address: {queryAddress}</Text>
      <TableContainer bg="blue.800" color="white">
        <Table variant="simple" fontSize="md">
          <Thead>
            <Tr bg="blue.200">
              <Th>txHash</Th> <Th>index</Th> <Th>value</Th>
            </Tr>
          </Thead>
          <Tbody>
            {data.utxos?.map((addr: any) => (
              <Tr>
                <Td> <pre> {addr?.txHash}</pre> </Td>
                <Td>
                  <pre>{addr?.index}</pre>
                </Td>
                <Td>{addr?.value}</Td>
              </Tr>              
            ))}
          </Tbody>
        </Table>
      </TableContainer>
    </Box>
  );
}
